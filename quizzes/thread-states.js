quizThreadStates = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Which state for what?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about states of threads.",
            "a": [
                {"option": "When a thread invokes a system call, it gets blocked.", "correct": false},
                {"option": "Blocked threads are not considered by the scheduler.", "correct": true},
                {"option": "In reaction to external events, the OS may unblock threads.", "correct": true},
                {"option": "At most one thread can be running at any point in time.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No.</span> Please check out <a href=\"Operating-Systems-Threads.html#slide-java-threads\">earlier slides</a>.</p>" // no comma here
        }
    ]
};
