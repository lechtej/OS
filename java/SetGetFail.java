public class SetGetFail implements Runnable {
    // Every thread is assigned a number and has a reference to a SharedObject.
    // In main(), a single SharedObject is passed to all threads.
    int number;
    SharedObject shared;

    public SetGetFail(int no, SharedObject so) {
	number = no;
	shared = so;
    }

    public static void main(String[] args) {
	SharedObject shared = new SharedObject(0);
        new Thread(new SetGetFail(1, shared)).start();
        new Thread(new SetGetFail(2, shared)).start();
    }

    synchronized public void run() {
        setGet();
    }

    synchronized void setGet() {
	// Repeatedly assign this thread's own number to the shared
	// object and race to read that number again.
	// Exit, if some other thread modified the number in between.
	while(true) {
	    shared.setNo(number);
	    int no = shared.getNo();
	    if (no != number) {
		System.out.println("Thread " + number + " sees " + no);
		System.exit(no);
	    }
	}
    }
}

class SharedObject {
    // Nothing special here.  Just store an int value, with getter and setter.
    private int number;
    public SharedObject (int no) { number = no; }
    public int getNo() { return number; }
    public void setNo(int no) { number = no; }
}
