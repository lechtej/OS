/*
  Code to demonstrate races.

  Copyright (C) 2018 Jens Lechtenbörger
  Licensed under GPLv3 and CC BY-SA 4.0.

  The body of method Theater.sell() is based on code from Sec. 4.2 of
  this book: Max Hailperin, Operating Systems and Middleware – Supporting Controlled Interaction, https://gustavus.edu/mcs/max/os-book/, CC BY-SA 3.0.
*/
public class TheaterEx { // Just a main method.
    private static final int noThreads = 100;
    private static final int noSeats = 50;
    public static void main(String[] args) {
        Theater theater = new Theater(noSeats);

        // Create noThreads customers, each of which will try to buy a ticket
        // at the same theater.  Clearly, this should not be possible if
        // noThreads > noSeats.
        Thread[] customers = new Thread[noThreads];
        for (int i=0; i<noThreads; i++)
            customers[i] = new Thread(new Customer(theater));

        // Start the customers.
        // To the reader: Carefully check the output messages.  Where do you
        // see the first surprise (if any)?
        // (Sample output is included as comment below.)
        for (int i=0; i<noThreads; i++)
            customers[i].start();
    }
}

class Customer implements Runnable { // Each customer tries to buy one seat.
    private Theater theater;
    public Customer(Theater theater) {
        this.theater = theater;
    }
    public void run() {
        int remaining = theater.sell();
        if (remaining == -1) System.out.println("All tickets sold out.");
        else System.out.println("Got ticket.  Remaining: " + remaining);
        }
}

class Theater { // Instance as shared resource, with some remaining seats.
    int seatsRemaining;

    public int sell() {
        if (seatsRemaining > 0) {
            dispenseTicket();
            seatsRemaining = seatsRemaining - 1;
            return seatsRemaining;
        } else {
            displaySorrySoldOut();
            return -1;
        }
    }
    public Theater(int seats) { seatsRemaining = seats; }
    private void dispenseTicket() { System.out.println("Selling ticket when " + seatsRemaining + " are remaining."); }
    private void displaySorrySoldOut() { System.out.println("Sorry, everything sold out."); }
}

/* Sample output with OpenJDK 1.8.0_161 and two processors in virtual
   machine with kernel Linux 4.9.
$ java TheaterEx
Selling ticket when 50 are remaining.
Got ticket.  Remaining: 49
Selling ticket when 50 are remaining.
Selling ticket when 50 are remaining.
Got ticket.  Remaining: 48
Selling ticket when 49 are remaining.
Got ticket.  Remaining: 47
Selling ticket when 48 are remaining.
Got ticket.  Remaining: 46
Got ticket.  Remaining: 45
Selling ticket when 47 are remaining.
Got ticket.  Remaining: 44
Selling ticket when 44 are remaining.
Got ticket.  Remaining: 43
Selling ticket when 44 are remaining.
Got ticket.  Remaining: 42
Selling ticket when 43 are remaining.
Got ticket.  Remaining: 41
Selling ticket when 41 are remaining.
Got ticket.  Remaining: 40
Selling ticket when 40 are remaining.
Got ticket.  Remaining: 39
Selling ticket when 40 are remaining.
Got ticket.  Remaining: 38
Selling ticket when 40 are remaining.
Got ticket.  Remaining: 37
Selling ticket when 40 are remaining.
Selling ticket when 40 are remaining.
Got ticket.  Remaining: 35
Got ticket.  Remaining: 36
Selling ticket when 38 are remaining.
Selling ticket when 39 are remaining.
Got ticket.  Remaining: 33
Got ticket.  Remaining: 34
Selling ticket when 33 are remaining.
Got ticket.  Remaining: 32
Selling ticket when 35 are remaining.
Selling ticket when 33 are remaining.
Got ticket.  Remaining: 30
Got ticket.  Remaining: 31
Selling ticket when 30 are remaining.
Got ticket.  Remaining: 29
Selling ticket when 29 are remaining.
Got ticket.  Remaining: 28
Selling ticket when 28 are remaining.
Got ticket.  Remaining: 27
Selling ticket when 27 are remaining.
Got ticket.  Remaining: 26
Selling ticket when 26 are remaining.
Got ticket.  Remaining: 25
Selling ticket when 25 are remaining.
Got ticket.  Remaining: 24
Selling ticket when 24 are remaining.
Got ticket.  Remaining: 23
Selling ticket when 23 are remaining.
Got ticket.  Remaining: 22
Selling ticket when 22 are remaining.
Got ticket.  Remaining: 21
Selling ticket when 22 are remaining.
Got ticket.  Remaining: 20
Selling ticket when 21 are remaining.
Got ticket.  Remaining: 19
Selling ticket when 19 are remaining.
Got ticket.  Remaining: 18
Selling ticket when 20 are remaining.
Got ticket.  Remaining: 17
Selling ticket when 20 are remaining.
Got ticket.  Remaining: 16
Selling ticket when 18 are remaining.
Got ticket.  Remaining: 15
Selling ticket when 19 are remaining.
Selling ticket when 19 are remaining.
Got ticket.  Remaining: 13
Got ticket.  Remaining: 14
Selling ticket when 13 are remaining.
Got ticket.  Remaining: 12
Selling ticket when 12 are remaining.
Got ticket.  Remaining: 11
Selling ticket when 11 are remaining.
Got ticket.  Remaining: 10
Selling ticket when 10 are remaining.
Got ticket.  Remaining: 9
Selling ticket when 9 are remaining.
Got ticket.  Remaining: 8
Selling ticket when 8 are remaining.
Got ticket.  Remaining: 7
Selling ticket when 7 are remaining.
Got ticket.  Remaining: 6
Selling ticket when 7 are remaining.
Got ticket.  Remaining: 5
Selling ticket when 5 are remaining.
Got ticket.  Remaining: 4
Selling ticket when 5 are remaining.
Got ticket.  Remaining: 3
Selling ticket when 5 are remaining.
Got ticket.  Remaining: 2
Selling ticket when 2 are remaining.
Got ticket.  Remaining: 1
Selling ticket when 5 are remaining.
Selling ticket when 1 are remaining.
Got ticket.  Remaining: 0
Selling ticket when 5 are remaining.
All tickets sold out.
Selling ticket when -1 are remaining.
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -3
Selling ticket when -3 are remaining.
Got ticket.  Remaining: -4
Selling ticket when -4 are remaining.
Got ticket.  Remaining: -5
Selling ticket when -5 are remaining.
Got ticket.  Remaining: -6
Selling ticket when -6 are remaining.
Selling ticket when 5 are remaining.
Selling ticket when 5 are remaining.
Got ticket.  Remaining: -9
Selling ticket when 5 are remaining.
Got ticket.  Remaining: -11
Got ticket.  Remaining: -10
Got ticket.  Remaining: -8
Got ticket.  Remaining: -7
Selling ticket when -7 are remaining.
Sorry, everything sold out.
All tickets sold out.
Selling ticket when -7 are remaining.
Selling ticket when -7 are remaining.
Got ticket.  Remaining: -2
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -15
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -16
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -17
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -18
Selling ticket when -1 are remaining.
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -20
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -21
Selling ticket when -1 are remaining.
Got ticket.  Remaining: -22
Selling ticket when 0 are remaining.
Got ticket.  Remaining: -23
Selling ticket when 0 are remaining.
Selling ticket when 0 are remaining.
Got ticket.  Remaining: -25
Got ticket.  Remaining: -24
Selling ticket when 0 are remaining.
Got ticket.  Remaining: -26
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -27
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -28
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -29
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -30
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -31
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -32
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -33
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -34
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -35
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -36
Selling ticket when 1 are remaining.
Got ticket.  Remaining: -37
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -38
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -39
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -40
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -41
Selling ticket when 3 are remaining.
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -42
Selling ticket when 3 are remaining.
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -45
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -46
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -47
Selling ticket when 3 are remaining.
Got ticket.  Remaining: -48
Got ticket.  Remaining: -44
Got ticket.  Remaining: -43
Got ticket.  Remaining: -19
Got ticket.  Remaining: -14
Got ticket.  Remaining: -13
Got ticket.  Remaining: -12
Sorry, everything sold out.
All tickets sold out.
*/
