# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "config-course.org"
#+OPTIONS: toc:1

#+TITLE: OS Overview

* Introduction
** Recall: Big Picture of CSOS
   - Computer Structures and Operating Systems (CSOS)
     #+ATTR_REVEAL: :frag (gray-out appear)
     - CS: How to build a computer from logic gates?
       {{{reveallicense("./figures/gates/nand.meta","6vh")}}}
       - Von Neumann architecture
       - CPU (ALU), RAM, I/O
	 {{{reveallicense("./figures/devices/cpu.meta","12vh")}}}
     - OS: What *abstractions* do Operating Systems provide for
       applications?
       - Processes and threads with scheduling and concurrency, virtual memory
         {{{reveallicense("./figures/screenshots/pong.meta","25vh")}}}
       - What is currently executing why and where, using what
         resources how?

*** OS Responsibilities
    {{{revealimg("./figures/external-non-free/jvns.ca/OS-responsibilities.meta",t,"50vh")}}}

** Prerequisite Knowledge
   - Be able to write, compile, and execute small /Java/ programs
     - What is an object?  What is the meaning of ~this~ in Java?
     - How do you execute a program that requires a command line argument?
   - Be able to explain basic /data structures/ (stacks, queues, trees) and
     /algorithms/ (in particular, hashing)
   - Being able to explain the database /transaction concept/ and
     /update anomalies/

* OS Teaching
** OS Part
   - Sharp cut ahead
     - So far, we followed a book with projects to build a computer bottom up
     - Unfortunately, nothing similar exists to build an Operating System
       (OS) bottom up
       - (Or is far too technical for our purposes)
   #+ATTR_REVEAL: :frag appear
   - Thus, the OS part presents major concepts and techniques of modern OSs
     - Some topics build upon each other
     - Some are more isolated
     - Goal: Understand and control your devices
       - Performance
       - Trust

** OS Exercises
   - While the CS part had weekly projects, this will *not* be the
     case for the OS part
     - Exercise work (with pen and paper, games, simulators, programs,
       shell) will be part of JiTT assignments and in-class meetings

** Textbook(s)
   - Major source (adopted in 2017)
     - cite:Hai17 [[https://gustavus.edu/mcs/max/os-book/][Max Hailperin: Operating Systems and Middleware: Supporting Controlled Interaction]]
       - Distributed under
         [[https://creativecommons.org/licenses/by-sa/3.0/][CC BY-SA 3.0]]
	 - Source code at [[https://github.com/Max-Hailperin/Operating-Systems-and-Middleware--Supporting-Controlled-Interaction][GitHub]]
       - PDF (also) in Learnweb
   - Additions
     - cite:Sta14 Stallings: Operating Systems -- Internals and Design
       Principles, 8/e, 2014
     - cite:TB15 Tanenbaum, Bos: Modern operating systems, 4th edition, 2015

** Presentations (1/2)
   - HTML presentations with audio for self-study
     - Read [[https://oer.gitlab.io/OS/index.html#hints][usage hints]] first
   - HTML generated from simple text files in [[https://orgmode.org/][Org mode]]
     - [[https://gitlab.com/oer/OS][Source code available on GitLab]]
       - You may want to check out those for your annotations
     - [[https://oer.gitlab.io/OS][Generated presentations]]
       - These are draft versions until you see a link in Learnweb
     - Offline use possible
       - Generate yourself or
         [[https://gitlab.com/oer/OS/pipelines][download from latest GitLab pipeline]]
     - Open Educational Resources (OER), your contributions are very welcome!

** Presentations (2/2)
   - As usual, (pointers to) presentations are available in Learnweb
   - Presentations include
     - Learning objectives
     - Explanations for class topics
     - JiTT assignments
       - More on [[file:Operating-Systems-JiTT.org][JiTT]] in a couple of minutes
     - Slides for in-class meetings
       - Topics that I plan to discuss in-class (after your questions
         have been answered)
     #+ATTR_REVEAL: :frag appear :audio ./audio/1-presentation-hints.ogg
     - Some slides contain audio explanations, e.g., the one starting
       with this bullet point
#+BEGIN_NOTES
Starting with the final bullet point of this slide, audio controls
should have appeared on the lower left, with audio playback starting
automatically.  The folder icon on the upper right indicates that a
transcript of the audio is available.

You read the usage hints for this type of presentations as instructed
on the previous slide, didn’t you?
#+END_NOTES


* Outlook on Major OS Concepts
** I/O
   - OS manages *input/output* (I/O) operations
     - Access to hardware
       - E.g., keyboard, disk, network
   #+ATTR_REVEAL: :frag appear
   - Modern CPUs support interrupts
     - I/O devices use *interrupts* to *signal* I/O events
       - CPU executes OS code of *interrupt handler*
     → Presentation on [[file:Operating-Systems-Interrupts.org][interrupts]]

** Manage Computations
   - We start programs/apps
   - The OS manages *processes*
     - Process = OS management unit
       - Abstraction for computation
         - With resources
       - Processes *isolated* from each other
     #+ATTR_REVEAL: :frag appear
     - Potentially, each process with multiple *threads*
     - Thread = another OS management unit
       - Part of process
       - Defined by instructions to execute
	 - E.g., an online game: different threads for game AI(s), GUI
           events, network handling
       - Threads of same process *share* resources

*** Threads
    - OS needs to manage threads
       - Housekeeping information, e.g.:
	  - What did each thread do the last time it was on a CPU core?
	     - What is the next instruction to execute?
	     - In what state (registers, stack, I/O)?
       → Chapter 2 of cite:Hai17, presentation on [[file:Operating-Systems-Threads.org][threads]]
    #+ATTR_REVEAL: :frag appear
    - OS needs to allocate CPU (cores) to different threads
       - (CPU) Scheduling
       → Chapter 3 of cite:Hai17, presentation on [[file:Operating-Systems-Scheduling.org][scheduling]]

*** CPU Scheduling
    {{{revealimg("./figures/external-non-free/jvns.ca/scheduling.meta",t,"50vh")}}}

** Controlling Concurrency
   - Processes and threads may need to *interact*
     #+ATTR_REVEAL: :frag (appear)
     - E.g., some producing data, others consuming
       - *Shared resources*
       - Prevent consumer from getting ahead of producer
       - Prevent producers from overwriting their data
     - Proper relative *timing* is crucial
     - Need *synchronization*
       - Recall ACID transactions and locking in DBMS
       - OS concept: *Mutual exclusion* (MX)

   #+ATTR_REVEAL: :frag appear
   → Chapter 4 of cite:Hai17, presentations on
     [[file:Operating-Systems-MX.org][MX]], [[file:Operating-Systems-MX-Java.org][MX in Java]], [[file:Operating-Systems-MX-Challenges.org][MX challenges]]

** Virtual Memory
   :PROPERTIES:
   :CUSTOM_ID: virtual-address-space
   :END:
   - OS manages *main memory* (RAM)
   #+ATTR_REVEAL: :frag (appear)
   - OS introduces layer of indirection/abstraction
     - Physical addresses (those seen in Hack)
     - Logical/virtual addresses
       - Each process has its own separate *virtual address space*, starting at
	 address 0
       - OS keeps track where each virtual address is really located in RAM
	 (if at all)
   - Benefits include
     - *Protection*: Threads of one process can neither “steal” nor “destroy”
       data of threads in another process
     - *Flexibility*: Programmers do not need to manage main memory

   #+ATTR_REVEAL: :frag appear
   → Chapter 6 of cite:Hai17, presentations
   [[file:Operating-Systems-Memory-I.org][Virtual Memory I]]
   and [[file:Operating-Systems-Memory-II.org][Virtual Memory II]]

** Processes
   - OS unit of management and protection
     - Address space per process
       - Shared by threads of process
   - Files for inter-process communication

   → Chapters 7, 8 of cite:Hai17, presentation on
   [[file:Operating-Systems-Processes.org][processes]]

** Security
   - IT systems are under attack
   - Attacks threaten security goals
   - Security mechanisms protect against attacks

   → Chapter 11 of cite:Hai17, presentation on
   [[file:Operating-Systems-Security.org][security]]

** Big Picture of OS Sessions
   {{{reveallicense("./figures/OS/2019-OS-overview-anim-plain.meta","50vh",nil,none,t)}}}

#+MACRO: copyrightyears 2017, 2018, 2019
#+INCLUDE: "~/.emacs.d/oer-reveal-org/backmatter.org"
